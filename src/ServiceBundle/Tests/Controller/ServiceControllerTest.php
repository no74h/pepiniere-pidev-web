<?php

namespace ServiceBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServiceControllerTest extends WebTestCase
{
    public function testAfficher()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/afficher');
    }

    public function testSabonner()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/sabonner');
    }

    public function testMesabonnements()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/mesabonnements');
    }

    public function testDesabonnement()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/desabonnement');
    }

}
