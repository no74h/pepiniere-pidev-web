<?php

namespace ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adresse
 *
 * @ORM\Table(name="adresse")
 * @ORM\Entity(repositoryClass="ServiceBundle\Repository\AdresseRepository")
 */
class Adresse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="gouvernorat", type="string", length=255)
     */
    private $gouvernorat;

    /**
     * @var int
     *
     * @ORM\Column(name="CodePoste", type="integer")
     */
    private $codePoste;

    /**
     * @var string
     *
     * @ORM\Column(name="citee", type="string", length=255)
     */
    private $citee;

    /**
     * @var string
     *
     * @ORM\Column(name="rue", type="string", length=255)
     */
    private $rue;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gouvernorat
     *
     * @param string $gouvernorat
     *
     * @return Adresse
     */
    public function setGouvernorat($gouvernorat)
    {
        $this->gouvernorat = $gouvernorat;

        return $this;
    }

    /**
     * Get gouvernorat
     *
     * @return string
     */
    public function getGouvernorat()
    {
        return $this->gouvernorat;
    }

    /**
     * Set codePoste
     *
     * @param integer $codePoste
     *
     * @return Adresse
     */
    public function setCodePoste($codePoste)
    {
        $this->codePoste = $codePoste;

        return $this;
    }

    /**
     * Get codePoste
     *
     * @return int
     */
    public function getCodePoste()
    {
        return $this->codePoste;
    }

    /**
     * Set citee
     *
     * @param string $citee
     *
     * @return Adresse
     */
    public function setCitee($citee)
    {
        $this->citee = $citee;

        return $this;
    }

    /**
     * Get citee
     *
     * @return string
     */
    public function getCitee()
    {
        return $this->citee;
    }

    /**
     * Set rue
     *
     * @param string $rue
     *
     * @return Adresse
     */
    public function setRue($rue)
    {
        $this->rue = $rue;

        return $this;
    }

    /**
     * Get rue
     *
     * @return string
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Adresse
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return int
     */
    public function getNumero()
    {
        return $this->numero;
    }
}

