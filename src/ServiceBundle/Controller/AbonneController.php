<?php

namespace ServiceBundle\Controller;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use ServiceBundle\Entity\Abonnement;
use ServiceBundle\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AbonneController extends Controller
{
    public function sabonnerAction(Request $request)
    {
        $user = $this->getUser();
        $em= $this->getDoctrine()->getManager();



        $ser= $em->getRepository(Services::class)->find($request->get('ids'));

        $abo= $em->getRepository(Abonnement::class)->findOneBy(['user'=>$user,'services'=>$ser]);
        if($abo==null){

            \Stripe\Stripe::setApiKey("sk_test_hcOTOjK1hTfslqAG7Wu4NELv00hQPccrhI");

            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token = $_POST['stripeToken'];
            $cout=0;
            switch ($request->get('type')){
                case '1': $cout = $ser->getTarif(); break;
                case '2': $cout = $ser->getTarif()*3; break;
                case '3': $cout = $ser->getTarif()*6; break;
                case '4': $cout = $ser->getTarif()*12; break;
                case 'select': break;
            }
            $cout=$cout*100;
            $charge = \Stripe\Charge::create([
                'amount' => $cout,
                'currency' => 'usd',
                'description' => 'user id:'.$user->getId().'nom et prenom'.$user->getNom().' '.$user->getPrenom().' service id: '. $ser->getId(). ' nom ser'.$ser->getNom() ,
                'source' => $token,
            ]);

            $abo= new Abonnement();
            $abo->setServices($ser);
            $abo->setUser($user);

            try {
                $abo->setDateDebut(new \DateTime($request->get('dated')));
                $abo->setDateFin(new \DateTime($request->get('datef')));
            } catch (\Exception $e) {
            }

            $em->persist($abo);
            $em->flush();

        }else{
            $asd=0;
        }
        return $this->redirectToRoute('service_mesabonnements');
    }

    public function mesAbonsAction(Request $request)
    {
        $user = $this->getUser();
        $em= $this->getDoctrine()->getManager();
        $mesabos= $em->getRepository(Abonnement::class)->findBy(['user'=>$user]);
        return $this->render('@Service/Abonne/afficher.html.twig', array(
            'abons'=>$mesabos
        ));
    }

    public function renouvlerAction(Request $request){

        $user = $this->getUser();
        $em= $this->getDoctrine()->getManager();
        $abo= $em->getRepository(Abonnement::class)->find($request->get('ids'));



        if($abo==null){

            \Stripe\Stripe::setApiKey("sk_test_hcOTOjK1hTfslqAG7Wu4NELv00hQPccrhI");

            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token = $_POST['stripeToken'];
            $cout=0;
            switch ($request->get('type')){
                case '1': $cout = $abo->getServices()->getTarif(); break;
                case '2': $cout = $abo->getServices()->getTarif()*3; break;
                case '3': $cout = $abo->getServices()->getTarif()*6; break;
                case '4': $cout = $abo->getServices()->getTarif()*12; break;
                case 'select': break;
            }
            $cout=$cout*100;
            $charge = \Stripe\Charge::create([
                'amount' => $cout,
                'currency' => 'usd',
                'description' => 'user id:'.$user->getId().'nom et prenom'.$user->getNom().' '.$user->getPrenom().' service id: '. $abo->getServices()->getId(). ' nom ser'.$abo->getServices()->getNom() ,
                'source' => $token,
            ]);

            try {
                $abo->setDateDebut(new \DateTime($request->get('dated')));
                $abo->setDateFin(new \DateTime($request->get('datef')));
            } catch (\Exception $e) {
            }


            $em->flush();

        }else{
            $asd=0;
        }
        return $this->redirectToRoute('service_mesabonnements');
    }
    public function desabonneeAction(Request $request){
        $user = $this->getUser();
        $em= $this->getDoctrine()->getManager();

        $abo= $em->getRepository(Abonnement::class)->find($request->get('ids'));

        $em->remove($abo);
        $em->flush();

        return $this->redirectToRoute('service_mesabonnements');

    }

}
