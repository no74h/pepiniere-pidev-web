<?php

namespace ServiceBundle\Controller;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use ServiceBundle\Entity\Abonnement;
use ServiceBundle\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ServiceController extends Controller
{
    public function afficherAction()
    {
        $em= $this->getDoctrine()->getManager();
        $services= $em->getRepository(Services::class)->findAll();
        return $this->render('@Service/Service/afficher.html.twig', array(
            'services'=>$services
        ));
    }
    public function detailAction(Request $request)
    {
        $user = $this->getUser();
        $em= $this->getDoctrine()->getManager();


        $ser= $em->getRepository(Services::class)->find($request->get('id'));
        $abo= $em->getRepository(Abonnement::class)->findOneBy(['user'=>$user,'services'=>$ser]);
        $form = $this->createFormBuilder()->add('captcha', CaptchaType::class,array('font'=>'Consolas','as_url'=>true,'reload'=>true,'quality'=>1000,'length'=>4,'invalid_message'=>'Veuillez verifier le code '))->getForm();;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){

            \Stripe\Stripe::setApiKey("sk_test_hcOTOjK1hTfslqAG7Wu4NELv00hQPccrhI");

            // Token is created using Checkout or Elements!
            // Get the payment token ID submitted by the form:
            $token = $_POST['stripeToken'];
            $cout=0;
            switch ($request->get('type')){
                case '1': $cout = $ser->getTarif(); break;
                case '2': $cout = $ser->getTarif()*3; break;
                case '3': $cout = $ser->getTarif()*6; break;
                case '4': $cout = $ser->getTarif()*12; break;
                case 'select': break;
            }
            $cout=$cout*100;
            $charge = \Stripe\Charge::create([
                'amount' => $cout,
                'currency' => 'usd',
                'description' => 'user id:'.$user->getId().'nom et prenom'.$user->getNom().' '.$user->getPrenom().' service id: '. $ser->getId(). ' nom ser'.$ser->getNom() ,
                'source' => $token,
            ]);

            $abo= new Abonnement();
            $abo->setServices($ser);
            $abo->setUser($user);

            try {
                $abo->setDateDebut(new \DateTime($request->get('dated')));
                $abo->setDateFin(new \DateTime($request->get('datef')));
            } catch (\Exception $e) {
            }

            $em->persist($abo);
            $em->flush();
            return $this->redirectToRoute('service_mesabonnements');
        }

        return $this->render('@Service/Service/detail.html.twig', array(
            'ser'=>$ser,
            'form' => $form->createView(),
        ));
    }





}
