<?php

namespace ServiceBundle\Controller;

use ServiceBundle\Entity\Abonnement;
use ServiceBundle\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AdminAbonController extends Controller
{
    public function afficherAction(){

        return $this->render('@Service/AdminAbon/afficher.html.twig');
    }

    public function chercherAction(Request $request)
    {


        $length = $request->get('length');
        $length = $length && ($length!=-1)?$length:0;

        $start = $request->get('start');
        $start = $length?($start && ($start!=-1)?$start:0)/$length:0;

        $search = $request->get('search');
        $type = $request->get('opt');
        //var_dump($type);

        $filters = [
            'query' => @$search['value'],
            'type' => @$type
        ];
        $em=$this->getDoctrine()->getManager();
        $services = $em->getRepository(Abonnement::class)->search(
            $filters, $start, $length
        );


        $output = array(
            'data' => array(),
            'recordsFiltered' => count($em->getRepository(Abonnement::class)->search($filters, 0, false)),
            'recordsTotal' => count($em->getRepository(Abonnement::class)->search(array(), 0, false))
        );
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);



        foreach ($services as $service) {

            //$serializer->serialize($service, 'json');
            $output['data'][] = $serializer->normalize($service);
        }

        return new Response(json_encode($output), 200, ['Content-Type' => 'application/json']);
    }


}
