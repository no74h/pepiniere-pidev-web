<?php
/**
 * Created by PhpStorm.
 * User: Rusty
 * Date: 05/02/2018
 * Time: 23:08
 */
namespace MyApp\UserBundle\Controller;
class LiensController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller
{
    public function GoToHome()
    {
        return $this->render('@MyAppUser/template 2/teams.html.twig');

    }

    public function GoToAdminAction()
    {
        return $this->render('@MyAppUser/TemplateAdmin/index.html.twig');

    }

}