<?php

namespace ProduitBundle\Controller;

use ProduitBundle\Entity\Categorieproduit;
use ProduitBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Categorieproduit controller.
 *
 */
class CategorieproduitController extends Controller
{
    /**
     * Lists all Categorieproduit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


            $categories= [];
            $categopar= [];
            $categoriespqrents= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);

            foreach ($categoriespqrents as $cat){
                $categ=$em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>$cat]);

                $categories[$cat->getIdcategorieproduit()]=$categ;
                $categopar[$cat->getIdcategorieproduit()]=$cat->getNomcategorie();
            }

            //var_dump($categories);


        return $this->render('@Produit/Categorieproduit/index.html.twig', array(
            'categories' => $categories,
            'categopar'=> $categopar
        ));
    }

    /**
     * Deletes a Categorieproduit entity.
     *
     */

    public function deleteAction()
    {
        $categorie=$this->getDoctrine()->getRepository(Categorieproduit::class);
        $obj=$categorie->find($_GET['idcategorieproduit']);
        $categorie=$this->getDoctrine()->getManager();
        $categorie->remove($obj);
        $categorie->flush();
        return $this->redirectToRoute('categorie_index');

    }

    public function newAction(Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        //$produit=$em->getRepository(Produit::class);
        $categorieproduit= new Categorieproduit();
        $categories= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);

        if ($request->isMethod('POST')) {
            $categorieproduit->setNomcategorie($request->get('nomCategorie'));
            if ($request->get('Idcategorieparent')=='0'){
                $categorieproduit->setIdcategorieproduitparent(null);
            }else{
                $cat= $em->getRepository(Categorieproduit::class)->find($request->get('Idcategorieparent'));
                $categorieproduit->setIdcategorieproduitparent($cat);
            }
            $em->persist($categorieproduit);
            $em->flush();
            return $this->redirectToRoute('categorie_index');}

        return $this->render('@Produit/Categorieproduit/new.html.twig', array(
            'categorieproduit' => $categorieproduit,
            'categories'=>$categories
        ));
    }
    public function updateAction(Request $request,$idcate)
    {

        $em= $this->getDoctrine()->getManager();
        //$produit=$em->getRepository(Produit::class);

        $categorieproduit= $em->getRepository(Categorieproduit::class)->find($idcate);
        $categories= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);

        if ($request->isMethod('POST')) {
            $categorieproduit->setNomcategorie($request->get('nomCategorie'));
            if ($request->get('Idcategorieparent')=='0'){
                $categorieproduit->setIdcategorieproduitparent(null);
            }else{
                $cat= $em->getRepository(Categorieproduit::class)->find($request->get('Idcategorieparent'));
                $categorieproduit->setIdcategorieproduitparent($cat);
            }
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('categorie_index');}

        return $this->render('@Produit/Categorieproduit/edit.html.twig', array(
            'categorieproduit' => $categorieproduit,
            'categories'=>$categories
        ));
    }

    public function rechercheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $nomcategorie = $request->get('nomcategorie');

        $categories= [];
        $categopar= [];
        $result = $this->getDoctrine()->getManager()->getRepository(Categorieproduit::class)->advancedSearch($nomcategorie);

       // $categoriespqrents= $em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>null]);

        foreach ($result as $cat){
            $categ=$em->getRepository(Categorieproduit::class)->findby(['idcategorieproduitparent'=>$cat]);

            $categories[$cat->getIdcategorieproduit()]=$categ;
            $categopar[$cat->getIdcategorieproduit()]=$cat->getNomcategorie();
        }

        //var_dump($categories);

        return $this->render('@Produit/Categorieproduit/index.html.twig', array(
            'categories' => $categories,
            'categopar'=> $categopar,

        ));



    }





}
