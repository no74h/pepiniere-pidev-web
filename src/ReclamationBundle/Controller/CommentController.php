<?php

namespace ReclamationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends Controller
{
    public function modifierCommentAction($id,Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        // $produit = $this->getDoctrine()->getRepository('ProduitBundle:Produit')->find($id);
        $comment = $this->getDoctrine()->getRepository('ReclamationBundle:Commentaire')->find($id);
        if($request->isMethod('POST')){
          //  $comment = new Commentaire();
            $comment->setDescription($request->get('message'));
            $comment->setUser($user);
           // $commentaire->setProduit($produit);
            //$reclamation->set(1);

            //    var_dump($reclamation);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'votre commentaire a été modifier avec succées ...!');

            $url = $this->generateUrl('details_produit');

            return $this->redirect($url);
        }

        //var_dump($comment);

        return $this->render('@Reclamation/Default/updateComment.html.twig',array(
            'comment'=>$comment,
        ));

    }


    public function supprimerComAction($id,Request $request)
    {
        $comment = $this->getDoctrine()
            ->getRepository('ReclamationBundle:Commentaire')
            ->find($id);

        $em =$this->getDoctrine()->getManager();

        $em->remove($comment);
        $em->flush();

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Le Commentaire supprimer avec succées ...!');

        return $this->redirectToRoute('All_reclamation');
    }
}
