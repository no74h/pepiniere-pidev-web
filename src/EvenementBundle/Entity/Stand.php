<?php

namespace EvenementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stand
 *
 * @ORM\Table(name="stand")
 * @ORM\Entity(repositoryClass="EvenementBundle\Repository\StandRepository")
 */
class Stand
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idStand", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idstand;

    /**
     * @var integer
     *
     * @ORM\Column(name="numeroStand", type="integer", nullable=true)
     */
    private $numerostand;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity=Evenement::class)
     * @ORM\JoinColumn(name="idevenement", referencedColumnName="idEvenement")
     */
    private $idevenement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomEvenement", type="string", length=50, nullable=false)
     */
    private $nomevenement;

    /**
     *
     *  @ORM\ManyToOne(targetEntity=MyApp\UserBundle\Entity\User::class)
     *
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="nomParticipant", type="string", length=50, nullable=false)
     */
    private $nomparticipant;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomParticipant", type="string", length=50, nullable=false)
     */
    private $prenomparticipant;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=200, nullable=false)
     */
    private $image;

    /**
     * @return int
     */
    public function getIdstand()
    {
        return $this->idstand;
    }

    /**
     * @param int $idstand
     */
    public function setIdstand($idstand)
    {
        $this->idstand = $idstand;
    }

    /**
     * @return int
     */
    public function getNumerostand()
    {
        return $this->numerostand;
    }

    /**
     * @param int $numerostand
     */
    public function setNumerostand($numerostand)
    {
        $this->numerostand = $numerostand;
    }

    /**
     * @return int
     */
    public function getIdevenement()
    {
        return $this->idevenement;
    }

    /**
     * @param int $idevenement
     */
    public function setIdevenement($idevenement)
    {
        $this->idevenement = $idevenement;
    }

    /**
     * @return string
     */
    public function getNomevenement()
    {
        return $this->nomevenement;
    }

    /**
     * @param string $nomevenement
     */
    public function setNomevenement($nomevenement)
    {
        $this->nomevenement = $nomevenement;
    }

    /**
     * @return int
     */


    /**
     * @param int $iduser
     */


    /**
     * @return string
     */
    public function getNomparticipant()
    {
        return $this->nomparticipant;
    }

    /**
     * @param string $nomparticipant
     */
    public function setNomparticipant($nomparticipant)
    {
        $this->nomparticipant = $nomparticipant;
    }

    /**
     * @return string
     */
    public function getPrenomparticipant()
    {
        return $this->prenomparticipant;
    }

    /**
     * @param string $prenomparticipant
     */
    public function setPrenomparticipant($prenomparticipant)
    {
        $this->prenomparticipant = $prenomparticipant;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }



}

